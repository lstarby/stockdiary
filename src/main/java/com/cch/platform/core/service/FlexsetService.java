package com.cch.platform.core.service;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.criterion.CriteriaSpecification;
import org.springframework.stereotype.Service;

import com.cch.platform.core.bean.CoreFlexset;
import com.cch.platform.core.bean.CoreFlexsetData;
import com.cch.platform.service.BaseService;
import com.cch.platform.util.StringUtil;

@Service
public class FlexsetService extends BaseService<CoreFlexset>{

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String,Object> getFlexsetValues(String flexsetCodes) {
		String[] codes=StringUtil.split2Array(flexsetCodes, ",");
		Map<String,Object> re=new HashMap<String,Object>(); 
		for(String code :codes){
			CoreFlexset flexset = this.get(code);
			String sql=flexset.getQuerySql();
			if("const".equals(flexset.getType())){
				Map params=new HashMap();
				params.put("flexsetCode", code);
				re.put(code, beanDao.find(CoreFlexsetData.class, params));
			}else{
				SQLQuery query = beanDao.getSession().createSQLQuery(sql);    
				query.setResultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP);
				re.put(code, query.list());
			}
		}
		return re;
	}
}
