$(function(){
	getReport();
});

function getReport(){
	$.post(plat.fullUrl("/report/moneyreport/query.do"),
			$('#ff').form("getData"),
			loadData
		);
}

function loadData(data){
	$('#ff').form("load",data);
	
	$('#container').highcharts('StockChart', {
            rangeSelector : {
                selected : 2
            },
            title : {
                text : '转账余额'
            },
            series : [{
                name : '余额',
                data : data.data,
                tooltip: {
                    valueDecimals: 2
                }
            }],
            xAxis: {  
                type: 'datetime',  
                labels: {  
                    step: 1,   
                    formatter: function () {  
                        return Highcharts.dateFormat('%m-%d', this.value);  
                    }  
                }  
            }     
        });
}