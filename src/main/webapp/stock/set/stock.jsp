<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<jsp:include page="/core/tool/resources.jsp"></jsp:include>
	<script src="./stock.js"></script>

</head>

<body>
	<div id="toolbar">
	   <a href="#" class="easyui-linkbutton" iconCls="icon-add" onclick="javascript:$('#dg').edatagrid('addRow')">新增</a>
       <a href="#" class="easyui-linkbutton" onclick="click_save()" iconCls='icon-save'>保存</a>
       <a href="#" class="easyui-linkbutton" onclick="click_delete()" iconCls="icon-remove">删除</a>
       <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" onclick="javascript:$('#dg').edatagrid('cancelRow')">取消</a>
	</div>
	<table id="dg" toolbar="#toolbar" style="width:100%"
		data-options="rownumbers:true,singleSelect:true,pagination:true,method:'post',idField:'stockCode',
			 	sortOrder:'asc', sortName:'stockCode', pageSize:20,
			 	fit:true, autoSave:true,
				url : plat.fullUrl('/stock/stockset/pagequery.do'),
				saveUrl:plat.fullUrl('/stock/stockset/save.do'),
				destroyUrl:plat.fullUrl('/stock/stockset/delete.do')">
        <thead>
            <tr>
                <th data-options="field:'stockCode',width:100,sortable:true" editor="{type:'validatebox',options:{required:true}}">证券代码</th>
                <th data-options="field:'stockName',width:100" editor="{type:'validatebox',options:{required:true}}">证券名称</th>
                <th data-options="field:'stockType',width:100,flexset:'SD_STOCK_TYPE'" editor='combobox'>证券种类</th>
                <th data-options="field:'stockMarket',width:100,flexset:'SD_MARKET',editor:'combobox'" >证券市场</th>
            </tr>
        </thead>
    </table>
</body>
</html>
